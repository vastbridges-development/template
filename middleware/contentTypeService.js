/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Sets the content type to return to the browser
 * @param {String} $contentType the content type to see as the response
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.contentType = ($contentType, $fileName) => (ignore, $response, $next) => {
    // set the response type
    $response.set("Content-Type", $contentType);
    // check to see if the file name is defined
    if ($fileName) {
        $response.setHeader("Content-Disposition", "attachment; filename=" + $fileName);
    }
    // go to the next function
    return $next();
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * SUGAR FUNCTIONS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Sugar function for setting the content type to html.
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.html = ($fileName) => module.exports.contentType("text/html", $fileName);

/**
 * Sugar function for setting the content type to plain text.
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.txt = ($fileName) => module.exports.contentType("text/plain", $fileName);

/**
 * Sugar function for setting the content type to xml.
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.xml = ($fileName) => module.exports.contentType("text/xml", $fileName);

/**
 * Sugar function for setting the content type to a CSV file.
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.csv = ($fileName) => module.exports.contentType("text/csv", $fileName);

/**
 * Sugar function for setting the content type to json.
 * @param {String} [$fileName] an optional fileName to name the file for download
 * @return {Function}
 */
module.exports.json = ($fileName) => module.exports.contentType("application/json", $fileName);