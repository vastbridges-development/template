/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const routeService = require("../services/routeService.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * 301 redirects the browser to the specified location
 * @param {String} $location the url to redirect to
 * @returns {Function}
 */
const redirect = ($location) => (ignore, $response) => {
    return $response.redirect(301, $location);
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = routeService.route();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * DOMAIN REDIRECTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.use(($request, $response, $next) => {
    if ($request.hostname === "localhost") {
        return $next();
    }
    if ($request.originalUrl === "/health") {
        return $next();
    }
    return $next();
});

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 301 REDIRECTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.get("/sitemap.xml", redirect("/sitemap/index.xml"));
module.exports.get("/index.html", redirect("/"));
module.exports.get("/wp-login.php", redirect("/"));
module.exports.get("/login", redirect("/"));
module.exports.get("/wp-admin", redirect("/"));