/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const merge = require("merge");
const path = require("path");
const routeService = require("../services/routeService.js");
const templateService = require("../services/templateService.js");


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ROUTES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = routeService.route();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.use(($request, $response, $next) => {
    // check if the data property is there
    if ($request.data !== Object($request.data)) {
        $request.data = {};
    }
    if ($request.view !== Object($request.view)) {
        $request.view = {};
    }
    // override the method
    $response.render = ($view, $data) => {
        // create the options
        const filename = path.join(__dirname, "..", "views", $view);
        // write the template to the response
        $response.write(templateService.template(filename, merge({
            _query: $request.query || {},
            _body: $request.body || {},
            _session: $request.session || {},
            _view: $request.view || {}
        }, $data)));
        // end the response
        $response.end();
    };
    return $next();
});