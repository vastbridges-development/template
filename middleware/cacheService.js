/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const config = require("../config.js");
const url = require("url");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Sets the cache to the specified age defined
 * @param {Number} $age the number of seconds for the cache ot be valid
 * @return {Function}
 */
module.exports.cache = ($age) => (ignore, $response, $next) => {
    $response.setHeader("Cache-Control", "public, max-age=" + $age);
    $next();
};

/**
 * Sets the cache to the maximum age as defined by the configuration
 * @return {Function}
 */
module.exports.maxCache = () => module.exports.cache(config.cache.maxAge);

/**
 * Sets the cache to the minimum age (1 second)
 * @return {Function}
 */
module.exports.noCache = () => (ignore, $response, $next) => {
    $response.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
    $response.header("Expires", "-1");
    $response.header("Pragma", "no-cache");
    $next();
};