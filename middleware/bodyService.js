/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const bodyParser = require("body-parser");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Parses the body of the request
 * @return {Function}
 */
module.exports.parse = () => bodyParser.urlencoded({
    extended: false
});

/**
 * Parses the body of the request
 * @return {Function}
 */
module.exports.json = () => bodyParser.json();