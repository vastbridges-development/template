/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// handles throwing out the exception pages
module.exports = ($error, $request, $response, $next) => {
    // set the response type
    $response.set("Content-Type", "text/html");
    // check for error
    console.error($request.url);
    console.error($error);
	// show the status pages
    $response.status($error.status || 500);
    return $response.render("500.pug");
};