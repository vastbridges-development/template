/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const cacheService = require("./middleware/cacheService.js");
const routeService = require("./services/routeService.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = routeService.route();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CUSTOM MIDDLEWARE
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.use(require("./middleware/redirectService.js"));
module.exports.use(require("./middleware/securityService.js"));
module.exports.use(require("./middleware/staticService.js"));
module.exports.use(require("./middleware/viewService.js"));

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ROUTES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.use("/", require("./routes/default.js"));
module.exports.use("/health", require("./routes/health.js"));

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 404 HANDLER
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// handle any files not found here
module.exports.use("*", cacheService.noCache());
module.exports.use("*", (ignore, $response) => {
    // set the response type
    $response.set("Content-Type", "text/html");
    $response.status(404);
    return $response.render("404.pug");
});

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 500 HANDLER
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// handle the exceptions
module.exports.use(require("./middleware/exceptionService.js"));