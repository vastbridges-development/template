/* jshint esversion: 6, node: true */

"use strict";


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const cacheService = require("../middleware/cacheService.js");
const contentTypeService = require("../middleware/contentTypeService.js");
const routeService = require("../services/routeService.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = routeService.route();


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ROUTES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.get("/", cacheService.maxCache());
module.exports.get("/", contentTypeService.html());
module.exports.get("/", ($request, $response) => $response.render("default.pug", {
	cards: [
		{
			id: 1,
			image: "/img/placeholder.svg",
			title: "Card #1",
			description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
			url: "/card/1"
		},
		{
			id: 2,
			image: "/img/placeholder.svg",
			title: "Card #2",
			description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
			url: "/card/2"
		},
		{
			id: 3,
			image: "/img/placeholder.svg",
			title: "Card #3",
			description: "Some quick example text to build on the card title and make up the bulk of the card's content.",
			url: "/card/3"
		}
	]
}));