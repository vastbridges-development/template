/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const bodyService = require("../middleware/bodyService.js");
const cacheService = require("../middleware/cacheService.js");
const contentTypeService = require("../middleware/contentTypeService.js");
const routeService = require("../services/routeService.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = routeService.route();

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * ROUTES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports.get("/", cacheService.noCache());
module.exports.get("/", contentTypeService.json());
module.exports.get("/", ($request, $response) => $response.json({
    health: true,
    date: new Date(),
    headers: $request.headers
}));

module.exports.post("/", cacheService.noCache());
module.exports.post("/", contentTypeService.json());
module.exports.post("/", bodyService.parse());
module.exports.post("/", ($request, $response) => $response.json({
    health: true,
    date: new Date(),
    headers: $request.headers
}));