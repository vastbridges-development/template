# BUILD INSTRUCTIONS #

In order to build the application you need the following tools available:

* NODEJS: [nodejs.org](http://nodejs.org)
* NPM (Included with NodeJS): [nodejs.org](http://nodejs.org)
* GIT: [git-scm.com](https://git-scm.com/downloads)

Once those are installed, you can clone the repo.

    git clone https://bitbucket.org/vastbridges-development/template.git

In order to build some of these npm packages on Windows you may need to install the build tools.  This can be done with NPM

    npm install --global windows-build-tools

This will make a folder called "template" containing all the source code.  Upon starting (you only ever need to do this once) enter the following commands (from the project root aka the "template" folder created earlier):

    npm install
    npm run build-js
    npm run build-css
    npm run build-img

In order to run the web server enter the following command.

    npm start
    
In order to package the application for deployment simply run the following command.

    npm run package