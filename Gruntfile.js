/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORT VARIABLES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const path = require("path");
const sass = require("node-sass");
const walk = require("walk");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Returns an object containing a reference to all the image files based found recursively in the
 * specified root directory
 * @param $root {String} the base directory to begin searching
 */
function imageList($root) {
    // hold the files that we will return
    const files = {};
    // use the walk command to synchronously grab the right files
    walk.walkSync($root, {
        listeners: {
            file: ($path, $stats, $next) => {
                // check to see if we match the extension?
                if (path.extname($stats.name) === ".jpg" || path.extname($stats.name) === ".jpeg" || path.extname($stats.name) === ".png" || path.extname($stats.name) === ".gif" || path.extname($stats.name) === ".svg") {
                    files[path.join($path, $stats.name)] = path.join($path, $stats.name);
                }
                // go to next file
                return $next();
            }
        }
    });
    // return the files
    return files;
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * The main grunt function to be called when the grunt command is run
 * @param {Object} $grunt a reference to the grunt library
 * @return {void}
 */
module.exports = ($grunt) => {

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * PRIVATE PROPERTIES
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    const project = $grunt.file.readJSON("package.json");

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * GRUNT COMMANDS
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $grunt.registerTask("default", "Must specify a task.", () => {
        // noinspection JSUnresolvedFunction
        $grunt.log.errorlns("Please specify a grunt task. See grunt --help");
    });
    $grunt.registerTask("build-css", "Concatenates and minifies CSS files.", ["sass", "copy", "cssmin"]);
    $grunt.registerTask("build-js", ["concat:app", "babel:app", "uglify:app"]);
    $grunt.registerTask("build-img", "Optimizes and minifies image files.", ["imagemin"]);
    $grunt.registerTask("package", "Builds the application without deploying it.", ["zip"]);

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * LOAD GRUNT MODULES
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    $grunt.loadNpmTasks("grunt-babel");
    $grunt.loadNpmTasks("grunt-contrib-concat");
    $grunt.loadNpmTasks("grunt-contrib-copy");
    $grunt.loadNpmTasks("grunt-contrib-cssmin");
    $grunt.loadNpmTasks("grunt-contrib-imagemin");
    $grunt.loadNpmTasks("grunt-contrib-uglify");
    $grunt.loadNpmTasks("grunt-sass");
    $grunt.loadNpmTasks("grunt-zip");

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * GRUNT CONFIGURATION
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    // noinspection JSUnresolvedFunction
    $grunt.initConfig({
        pkg: $grunt.file.readJSON("package.json"),
		babel: {
			options: {
				sourceMap: true,
				presets: ["env"]
			},
			app: {
			    files: {
			        "public/js/app.js": "public/js/app.js"
			    }
			}
		},
        concat: {
            app: {
                src: [
                    "node_modules/jquery/dist/jquery.js",
                    "node_modules/popper.js/popper.js",
                    "node_modules/bootstrap/dist/js/bootstrap.bundle.js"
                ],
                dest: "public/js/app.js"
            }
        },
        copy: {
            fontawesome: {
                files: [
                    {
                        expand: true,
                        cwd: "node_modules/font-awesome/fonts/",
                        src: "**",
                        dest: "public/fonts/",
                        filter: "isFile",
                        flatten: true
                    }
                ]
            }
        },
		cssmin: {
            options: {
                // sourceMap: false, // don"t want for production
                keepSpecialComments: 0,
                rebase: true
            },
            app: {
                files: {
                    "public/css/app.min.css": [
                        "public/css/app.css"
                    ]
                }
            }
		},
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 3
                },
                files: imageList("public/img")
            }
        },
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            app: {
                files: {
                    "public/css/app.css": "public/source/app.scss"
                }
            }
        },
        uglify: {
            options: {
                sourceMap: true
            },
            app: {
                files: {
                    "public/js/app.min.js": "public/js/app.js"
                }
            }
        },
        zip: {
            "package": {
                src: [
                    "config.js",
                    "index.js",
                    "package.json",
                    "package-lock.json",
                    "server.js",
                    "middleware/**/*",
                    "public/robots.txt",
                    "public/favicon.ico",
                    "public/css/**/*",
                    "public/fonts/**/*",
                    "public/img/**/*",
                    "public/js/**/*",
                    "routes/**/*",
                    "services/**/*",
                    "views/**/*"
                ],
                dest: path.join("package", project.version + ".zip")
            }
        }
    });
};
