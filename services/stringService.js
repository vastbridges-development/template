/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const crypto = require("crypto");
const promiseService = require("./promiseService.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Generates a random string with the specified bytes in length
 * @param {Number} $bytes the number of bytes to generate
 * @returns {Promise}
 */
module.exports.random = ($bytes) => promiseService.promise(($resolve, $reject) => crypto.randomBytes($bytes, ($error, $buffer) => {
    // check for error
    if ($error) {
        return $reject($error);
    }
    // return the buffer as a string
    return $resolve($buffer.toString("hex"));
}));