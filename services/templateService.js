/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const config = require("../config.js");
const merge = require("merge");
const pug = require("pug");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Indicates if the template should be built using the development features or not
 * @type {boolean}
 */
module.exports.development = (config.runtime === "DEVELOPMENT");

/**
 * Creates a template from a file
 * @param {String} $template the full path to the template
 * @param {Object} $data data to pass to the template
 * @return {String}
 */
module.exports.template = ($template, $data) => pug.compileFile($template, {
    filename: $template,
    pretty: module.exports.development,
    self: false,
    debug: false,
    compileDebug: false,
    cache: !module.exports.development
})(merge({
    _config: config
}, $data));
