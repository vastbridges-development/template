/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Creates a promise, calling the callback and passing the status functions
 * @param {Function} $callback the callback to call
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.promise = ($callback) => new Promise($callback);

/**
 * Creates a promise, calling the array of callbacks and passing the status functions when completed
 * @param {Array} $promises the promises to call
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.all = ($promises) => Promise.all($promises);

/**
 * Returns a promise that fulfills or rejects as soon as one of the promises in the iterable fulfills
 * or rejects, with the value or reason from that promise.
 * @param {Array} $promises the promises to call
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.race = ($promises) => Promise.race($promises);

/**
 * Returns a Promise object that is resolved with the given value.
 * @param {*} $data the data to pass
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.resolve = ($data) => Promise.resolve($data);

/**
 * Returns a Promise object that is rejected with the given reason.
 * @param {Error} $error the error reason
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.reject = ($error) => Promise.reject($error);

/**
 * Pauses execution for a certain amount of time.
 * @param {Number} $time the time to pause for
 * @returns {exports.promise|*|Promise|promise}
 */
module.exports.pause = ($time) => module.exports.promise(($resolve) => setTimeout(() => $resolve(), $time));