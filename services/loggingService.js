/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * IMPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const config = require("../config.js");

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE PROPERTIES
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

const doLog = ($type, $message, $date = new Date()) => {
    // check to see if we are in development mode or not
    if (module.exports.development) {
        console.log("[" + $type.toUpperCase() + "] " + $date.toLocaleDateString() + " " + $date.toLocaleTimeString() + ": " + JSON.stringify($message));
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Indicates if the logs should be run using the development features or not
 * @type {boolean}
 */
module.exports.development = (config.runtime === "DEVELOPMENT");

/**
 * Adds a standard log to the application logging framework
 * @param {*} $log the message to add is called with JSON.stringify()
 */
module.exports.log = ($log) => doLog("LOG", $log);

/**
 * Adds a information log to the application logging framework
 * @param {*} $log the message to add is called with JSON.stringify()
 */
module.exports.info = ($log) => doLog("INFO", $log);

/**
 * Adds an error log to the application logging framework
 * @param {*} $log the message to add is called with JSON.stringify()
 * @param {Error} [$error] optional error object
 */
module.exports.error = ($log, $error) => {
    doLog("ERROR", $log);
    doLog("ERROR", $error);
};

/**
 * Adds a warning log to the application logging framework
 * @param {*} $log the message to add is called with JSON.stringify()
 */
module.exports.warn = ($log) => doLog("WARN", $log);

/**
 * Adds a fatal log to the application logging framework
 * @param {*} $log the message to add is called with JSON.stringify()
 */
module.exports.fatal = ($log) => doLog("FATAL", $log);