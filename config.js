/* jshint esversion: 6, node: true */

"use strict";

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * PRIVATE METHODS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Returns the specified value of the environment parameter
 * @param $name {String} the name of the environmental variable
 * @param [$default] {String} the default value if no value is found
 * @returns {String}
 */
const env = ($name, $default) => process.env[$name] || $default || null;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * EXPORTS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = {
    cache: {
        maxAge: env("CACHE-MAXAGE", 1800)
    },
    cdnname: env("CDNNAME", "http://localhost:5000"),
    hostname: env("HOSTNAME", "http://localhost:5000"),
    port: env("PORT", 5000),
    runtime: env("ENVIRONMENT", "DEVELOPMENT")
};
